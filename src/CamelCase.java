import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CamelCase {
	
// Antes do nomeComposto	
//	public static List<String> converterCamelCase(String original) {
//		listaDePalavras.add(original.toLowerCase());
//		return listaDePalavras;
//	}
	
	public static List<String> converterCamelCase(String original) {
		List<String> listaDePalavras = new ArrayList<>();

		char letras[] = original.toCharArray();
		for(int i = 0; i < letras.length; i++) {
			if(Character.isUpperCase(letras[i])) {
				System.out.println(letras[i]);

				if(listaDePalavras.size() == 0 && i > 0)
					listaDePalavras.add(original.substring(0, i).toLowerCase());

				listaDePalavras.add(original.substring(i, original.length()).toLowerCase());
			}
		}
		

		if(listaDePalavras.isEmpty()){
			return Arrays.asList(original.toLowerCase());
		}
		return listaDePalavras;
	}
	
}
