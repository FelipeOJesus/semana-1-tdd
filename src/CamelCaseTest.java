import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CamelCaseTest {
	
	CamelCase camelCase;
	
	@Before
	public void inicializador() {
		camelCase = new CamelCase();
	}
	
	@Test
	public void camelCaseNomeMinusculo() {
		assertEquals("nome", CamelCase.converterCamelCase("nome").get(0));
	}
	
	@Test
	public void camelCaseNomeMaiusculo() {
		assertEquals("nome", CamelCase.converterCamelCase("Nome").get(0));
	}
	
	@Test
	public void publicoComposto() {

		String nomeComposto = "nomeComposto";

		List<String> camelCase = CamelCase.converterCamelCase(nomeComposto);

		assertEquals("nome", camelCase.get(0));
		assertEquals("composto", camelCase.get(1));
		
	}
	
}
